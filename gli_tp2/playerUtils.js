const rp = require('request-promise');
const _cliProgress = require('cli-progress');

const bar1 = new _cliProgress.SingleBar({}, _cliProgress.Presets.shades_classic);
var debug = false;

function getMyPlayerRatio(player, number) {
  return player.attack - number;
}

async function fight(opponent) {
  var multiplier;
  console.log("Generating a multiplier for player1 ...");
  if (!debug)
    bar1.start(100, 0);
  await getRandomValues().then(result => multiplier = result);
  if (!debug)
    bar1.stop();
  console.log("Multiplier successfully generated : " + multiplier);

  var player1Sub = getMyPlayerRatio(this, opponent.defense) * multiplier;
  console.log(this.name + " ratio : " + player1Sub);

  console.log("Generating a multiplier for player2 ...");
  if (!debug)
    bar1.start(100, 0);
  await getRandomValues().then(result => multiplier = result);
  if (!debug)
    bar1.stop();
  console.log("Multiplier successfully generated : " + multiplier);

  var player2Sub = getMyPlayerRatio(opponent, this.defense) * multiplier;
  console.log(opponent.name + " ratio : " + player2Sub);

  if (player1Sub > player2Sub) {
    console.log(this.name + "(" + player1Sub + ") > " + opponent.name + "(" + player2Sub + ")");
    this.hp -= (player2Sub * (this.hp / player1Sub));
    opponent.hp = 0;
    this.displayMyPlayerInfo();
  } else if (player1Sub < player2Sub) {
    console.log(this.name + "(" + player1Sub + ") < " + opponent.name + "(" + player2Sub + ")");
    opponent.hp -= (player1Sub * (opponent.hp / player2Sub));
    this.hp = 0;
    opponent.displayMyPlayerInfo();
  } else {
    console.log(this.name + "(" + player1Sub + ") = " + opponent.name + "(" + player2Sub + ")");
    this.hp -= (player2Sub * (this.hp / player1Sub));
    opponent.hp -= (player1Sub * (opponent.hp / player2Sub));
    console.log("No winner, everybody is dead.")
  }
}

function processArray(array) {
  var processedArray = array;
  if (debug)
    console.log("[FILTER-BEFORE] Array => " + processedArray);

  processedArray = processedArray.filter(function(number) {
    return number > 10;
  });
  if (debug) {
    console.log("[FILTER-AFTER] Array => " + processedArray);
    console.log("[MAP-BEFORE] Array => " + processedArray);
  }
  processedArray = processedArray.map(function(number, index) {
    return (number % 10 == index) ? number * 2 : number;
  });
  if (debug) {
    console.log("[MAP-AFTER] Array => " + processedArray);
    console.log("[REDUCE-BEFORE] Array => " + processedArray);
  }
  if (processedArray.length > 0) {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    processedArray = processedArray.reduce(reducer) / 100;
  } else {
    processedArray = 1;
  }
  if (debug)
    console.log("[REDUCE-AFTER] Array => " + processedArray);
  return processedArray;
}

async function getRandomValues() {
  var array = [];
  var rqvalue = 100;
  var min = 0;
  var ratio = 1.1;
  var arrayIndex = 0;
  for (let i = 0; i < 10; i++) {
    await getRandomNumber(min, rqvalue).then(result => array.push(result));
    if (!debug)
      bar1.update(10 * i + 10);
    arrayIndex = array.length - 1;
    if (debug)
      console.log("result[" + arrayIndex + "] " + array[arrayIndex]);
    rqvalue = Math.round(array[arrayIndex] * (ratio + 0.1));
    rqvalue = (rqvalue >= (min + 1)) ? rqvalue : (min + 1);
  }
  array = processArray(array);
  return array;
}

function getRandomNumber(min, max) {
  var options = {
    method: 'GET',
    uri: 'https://www.random.org/integers/?num=1&min=' + min + '&max=' + max + '&col=1&base=10&format=plain&rnd=new',
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true
  };

  return new Promise(function(resolve, reject) {
    rp(options, function(error, response) {
      if (error) {
        error("An error occured");
      } else {
        resolve(response.body);
      }
    })
  });
}

module.exports.getMyPlayerRatio = getMyPlayerRatio;
module.exports.fight = fight;
