var Player = require('./Player').Player;
/*
// Prototype part
function PayToWinPlayer() {
  this.name = name;
  this.attack = attack * 1.4;
  this.defense = defense;
  this.hp = 100;
}

PayToWinPlayer.prototype = Player.prototype;
*/
class PayToWinPlayer extends Player {
  constructor(name, attack, defense) {
    super(name, attack * 1.4, defense);
  }
}
module.exports.PayToWinPlayer = PayToWinPlayer;
