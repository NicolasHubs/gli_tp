var utils = require('./playerUtils.js');
/*
// Prototype part
function Player(name, attack, defense) {
  this.name = name;
  this.attack = attack;
  this.defense = defense;
  this.hp = 100;
}

Player.prototype.displayMyPlayerInfo = function() {
  console.log("My name is " + this.name + ", I have " + this.attack + " attack, " + this.defense + " defense and " + this.hp + " health points.");
};

Player.prototype.fight = utils.fight;
*/

class Player {
  constructor(name, attack, defense) {
    this.name = name;
    this.attack = attack;
    this.defense = defense;
    this.hp = 100;
  }

  displayMyPlayerInfo() {
    console.log("My name is " + this.name + ", I have " + this.attack + " attack, " + this.defense + " defense and " + this.hp + " health points.");
  }

  fight(opponent) {
    utils.fight.call(this, opponent);
  }
}

module.exports.Player = Player;
