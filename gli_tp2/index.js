//var generator = require('./playerGenerator.js');
var Player = require('./Player.js').Player;
var utils = require('./playerUtils.js');
//var p1 = generator.playerGenerator("Booba", 30, 5);
//var p2 = generator.playerGenerator("Kaaris", 28, 7);

var p1 = new Player("Booba", 30, 7);
var p2 = new Player("Kaaris", 30, 7);

p1.displayMyPlayerInfo();
p2.displayMyPlayerInfo();

p1.fight(p2);
