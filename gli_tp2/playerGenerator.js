var utils = require('./playerUtils.js');

function playerGenerator(name, attack, defense) {
  var hp = 100;

  function displayMyPlayerInfo() {
    console.log("My name is " + name + ", I have " + attack + " attack, " + defense + " defense and " + this.hp + " health points.")
  }

  var player = {
    name: name,
    attack: attack,
    defense: defense,
    hp: hp,
    displayPlayerInfo: displayMyPlayerInfo,
    fight: utils.fight
  }

  return player;
}

module.exports.playerGenerator = playerGenerator;
