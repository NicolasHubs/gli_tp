package fr.istic.gli.view;

import fr.istic.gli.controller.IController;
import fr.istic.gli.data.Pie;
import fr.istic.gli.model.ModelAdapter;

import javax.swing.table.AbstractTableModel;

public class MyTableModel extends AbstractTableModel {

    // a link to the Model interface
    private transient ModelAdapter model;

    // a link to the controller interface
    private transient IController controller;

    private String[] colNames = {"Title", "Value", "Description"};
    private Class[] colTypes = {String.class, Double.class, String.class};

    public MyTableModel(ModelAdapter model) {
        super();
        this.model = model;
    }

    @Override
    public int getRowCount() {
        return model.size();
    }

    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Pie pieSelected = model.getListPie().get(row);

        switch (col) {
            case 0:
                return pieSelected.getTitle();
            case 1:
                return pieSelected.getValue();
            case 2:
                return pieSelected.getDescription();
            default:
                break;
        }

        return "";
    }

    @Override
    public String getColumnName(int col) {
        return colNames[col];
    }

    @Override
    public Class getColumnClass(int col) {
        return colTypes[col];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object obj, int row, int col) {
        Pie pieSelected = model.getListPie().get(row);

        switch (col) {
            case 0:
                pieSelected.setTitle((String) obj);
                break;
            case 1:
                pieSelected.setValue((double) obj);
                break;
            case 2:
                pieSelected.setDescription((String) obj);
                break;
            default:
                break;
        }
    }

    public ModelAdapter getModel() {
        return model;
    }

    public void setModel(ModelAdapter model) {
        this.model = model;
    }

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

}


