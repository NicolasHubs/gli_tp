package fr.istic.gli.view;

import fr.istic.gli.controller.IController;
import fr.istic.gli.model.ModelAdapter;
import fr.istic.gli.observer.Observable;
import fr.istic.gli.observer.Observer;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MyJtable extends JTable implements Observer, MouseListener {

    private transient ModelAdapter modelAdapter;
    // a link to the controller interface
    private transient IController controller;

    public MyJtable(MyTableModel tableModel, ModelAdapter modelAdapter) {
        super(tableModel);
        this.modelAdapter = modelAdapter;
        this.modelAdapter.addObserver(this);
        addMouseListener(this);
    }

    public void previousPie() {
        int i = (((getSelectedRow() - 1)
                % getRowCount()) + getRowCount()) % getRowCount();
        setRowSelectionInterval(i, i);
        controller.setSelectedPie(i);
        System.out.println("Selected pie" + controller.getSelectedPie());
    }

    public void deSelect() {
        controller.setSelected(false);
        getSelectionModel().clearSelection();
    }

    public void selectPie(int i) {
        setRowSelectionInterval(i, i);
        controller.setSelected(true);
        controller.setSelectedPie(i);
        System.out.println("Selected pie" + i);
    }

    public void nextPie() {
        int i = (getSelectedRow() + 1)
                % getRowCount();
        setRowSelectionInterval(i, i);
        controller.setSelectedPie(i);
        System.out.println("Selected pie" + controller.getSelectedPie());
    }

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

    @Override
    public void update(Observable observable) {
        // Do nothing because : Auto-generated
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        controller.selectPie(getSelectedRow());
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        // Do nothing because : Auto-generated
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        // Do nothing because : Auto-generated
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        // Do nothing because : Auto-generated
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        // Do nothing because : Auto-generated
    }
}
