package fr.istic.gli.model;

import fr.istic.gli.data.Pie;

import java.util.ArrayList;
import java.util.List;

public class CamembertModel extends ModelAdapter {

    private String title;
    private transient List<Pie> pieList;
    private String unit;

    public CamembertModel(String title, String unit) {
        super();
        this.title = title;
        this.unit = unit;
        pieList = new ArrayList<>();
    }

    @Override
    public int size() {
        return pieList.size();
    }

    @Override
    public double getValues(int i) {
        Pie pie = pieList.get(i);
        return (pie != null) ? pie.getValue() : 0.0;
    }

    @Override
    public double total() {
        double total = 0.0;
        for (Pie pie : pieList)
            total += pie.getValue();
        return total;
    }

    @Override
    public String getTitle(int i) {
        Pie pie = pieList.get(i);
        return (pie != null) ? pie.getTitle() : "";
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public String getDescription(int i) {
        Pie pie = pieList.get(i);
        return (pie != null) ? pie.getDescription() : "";
    }

    @Override
    public void addPie(Pie pie) {
        if (null != pie) {
            pieList.add(pie);
        }
    }

    @Override
    public void removePie(Pie pie) {
        if (null != pie) {
            for (int i = 0; i < pieList.size(); i++) {
                if (pie.equals(pieList.get(i))) {
                    pieList.remove(i);
                    break;
                }
            }
        }
    }


    @Override
    public List<Pie> getListPie() {
        return pieList;
    }
}
