package fr.istic.gli.model;

import fr.istic.gli.data.Pie;

import java.util.List;

public interface IModel {
    int size();

    double getValues(int i);

    double total();

    String getTitle(int i);

    String getTitle();

    String getUnit();

    String getDescription(int i);

    void addPie(Pie pie);

    void removePie(Pie pie);

    List<Pie> getListPie();
}
