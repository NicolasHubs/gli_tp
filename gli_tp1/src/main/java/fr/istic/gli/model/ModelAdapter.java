package fr.istic.gli.model;

import fr.istic.gli.data.Pie;
import fr.istic.gli.observer.Observable;
import fr.istic.gli.observer.Observer;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class ModelAdapter extends AbstractTableModel implements IModel, Observable  {
    private CamembertModel model;

    private transient List<Observer> observerList;

    public ModelAdapter() {
        super();
    }

    public ModelAdapter(CamembertModel model) {
        super();
        observerList = new ArrayList<>();
        this.model = model;
    }

    @Override
    public void addObserver(Observer observer) {
        if (null != observer)
            observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyAllObserver() {
        for (Observer observer : observerList)
            observer.update(this);
    }

    @Override
    public int size() {
        return model.size();
    }

    @Override
    public double getValues(int i) {
        return model.getValues(i);
    }

    @Override
    public double total() {
        return model.total();
    }

    @Override
    public String getTitle(int i) {
        return model.getTitle(i);
    }

    @Override
    public String getTitle() {
        return model.getTitle();
    }

    @Override
    public String getUnit() {
        return model.getUnit();
    }

    @Override
    public String getDescription(int i) {
        return model.getDescription(i);
    }

    @Override
    public void addPie(Pie pie) {
        model.addPie(pie);
        notifyAllObserver();
    }

    @Override
    public void removePie(Pie pie) {
        model.removePie(pie);
        notifyAllObserver();
    }

    @Override
    public List<Pie> getListPie() {
        return model.getListPie();
    }


    // Jtable


    private int selectedRow;
    private String[] colNames = {"Title", "Value", "Description"};
    private Class[] colTypes = {String.class, Double.class, String.class};


    @Override
    public int getRowCount() {
        return size();
    }

    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Pie pieSelected = getListPie().get(row);
        selectedRow = row;
        switch (col) {
            case 0:
                return pieSelected.getTitle();
            case 1:
                return pieSelected.getValue();
            case 2:
                return pieSelected.getDescription();
            default:
                break;
        }

        return "";
    }

    @Override
    public String getColumnName(int col) {
        return colNames[col];
    }

    @Override
    public Class getColumnClass(int col) {
        return colTypes[col];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object obj, int row, int col) {
        Pie pieSelected = getListPie().get(row);

        switch (col) {
            case 0:
                pieSelected.setTitle((String) obj);
                break;
            case 1:
                pieSelected.setValue((double) obj);
                break;
            case 2:
                pieSelected.setDescription((String) obj);
                break;
            default:
                break;
        }
    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        // Do nothing because : Auto-generated
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {
        // Do nothing because : Auto-generated
    }

    public int getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
    }

}
