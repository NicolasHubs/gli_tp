package fr.istic.gli.controller;

import fr.istic.gli.model.IModel;

import java.awt.*;

public interface IController extends IModel {
    void setSelected(boolean b);

    int getSelectedPie();

    void setSelectedPie(int i);

    boolean isSelected();

    void selectPie(int i);

    void deSelect();

    void nextPie();

    void previousPie();

    Component getView();
}
