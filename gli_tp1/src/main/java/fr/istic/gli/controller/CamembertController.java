package fr.istic.gli.controller;

import fr.istic.gli.data.Pie;
import fr.istic.gli.model.IModel;
import fr.istic.gli.view.CamembertView;
import fr.istic.gli.view.MyJtable;

import java.awt.*;
import java.util.List;

public class CamembertController implements IController {

    private IModel model;
    private CamembertView view;
    private MyJtable myJtable;
    private boolean isSelected;
    private int pieSelected;

    public CamembertController() {
    }

    public CamembertController(IModel model, CamembertView view, MyJtable myJtable) {
        this.model = model;
        this.view = view;
        this.myJtable = myJtable;
        isSelected = false;
        pieSelected = 0;
    }

    @Override
    public void setSelected(boolean b) {
        isSelected = b;
    }

    @Override
    public int getSelectedPie() {
        return pieSelected;
    }

    @Override
    public void setSelectedPie(int i) {
        pieSelected = i;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void selectPie(int i) {
        view.selectPie(i);
        myJtable.selectPie(i);
    }

    @Override
    public void deSelect() {
        view.deSelect();
        myJtable.deSelect();
    }

    @Override
    public void nextPie() {
        view.nextPie();
        myJtable.nextPie();
    }

    @Override
    public void previousPie() {
        view.previousPie();
        myJtable.previousPie();
    }

    @Override
    public int size() {
        return model.size();
    }

    @Override
    public double getValues(int i) {
        return model.getValues(i);
    }

    @Override
    public double total() {
        return model.total();
    }

    @Override
    public String getTitle(int i) {
        return model.getTitle(i);
    }

    @Override
    public String getTitle() {
        return model.getTitle();
    }

    @Override
    public String getUnit() {
        return model.getUnit();
    }

    @Override
    public String getDescription(int i) {
        return model.getDescription(i);
    }

    @Override
    public void addPie(Pie pie) {
        model.addPie(pie);
    }

    @Override
    public void removePie(Pie pie) {
        model.removePie(pie);
    }

    @Override
    public List<Pie> getListPie() {
        return model.getListPie();
    }

    @Override
    public Component getView() {
        return view;
    }

    public IModel getModel() {
        return model;
    }

    public void setModel(IModel model) {
        this.model = model;
    }

    public void setView(CamembertView view) {
        this.view = view;
    }

    public MyJtable getMyJtable() {
        return myJtable;
    }

    public void setMyJtable(MyJtable myJtable) {
        this.myJtable = myJtable;
    }
}
