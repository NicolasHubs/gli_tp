package fr.istic.gli.data;

import java.util.Objects;

public class Pie {
    private String title;
    private double value;
    private String description;

    public Pie(String title, double value, String description) {
        this.title = title;
        this.value = value;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Pie))
            return false;
        Pie pie = (Pie) obj;

        return (title != null && title.equals(pie.getTitle()))
                && (description != null && description.equals(pie.getDescription()))
                && (value == pie.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, value, description);
    }
}
