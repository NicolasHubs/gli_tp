package fr.istic.gli.observer;

public interface Observer {
    void update(Observable observable);
}
