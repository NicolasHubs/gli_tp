package fr.istic.gli.demarrage;

import fr.istic.gli.controller.CamembertController;
import fr.istic.gli.controller.IController;
import fr.istic.gli.data.Pie;
import fr.istic.gli.model.CamembertModel;
import fr.istic.gli.model.ModelAdapter;
import fr.istic.gli.view.CamembertView;
import fr.istic.gli.view.MyJtable;
import fr.istic.gli.view.MyTableModel;

import javax.swing.*;
import java.awt.*;

public class Main {
    // this main method should actually be placed in another class (it's here just to avoid having multiple files)
    public static void main(String[] a) {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //window.setBounds(30, 30, 700, 700);

        CamembertModel camembertModel = new CamembertModel("Budget", "euros");
        // Create an instance of the model
        ModelAdapter modelAdapter = new ModelAdapter(camembertModel);

        Pie pie1 = new Pie("Mobile", 30, "Trop cher");
        Pie pie2 = new Pie("Cinéma", 10, "C'est ok");
        Pie pie3 = new Pie("Loyer", 65, "C'est vraiment pas mal");
        Pie pie4 = new Pie("Vélo", 22, "Nice");
        Pie pie5 = new Pie("Nourriture", 7, "Oh yeah");
        Pie pie6 = new Pie("Sport", 30, "La vache !");
        Pie pie7 = new Pie("Téléphone", 88, "Oh merde");
        modelAdapter.addPie(pie1);
        modelAdapter.addPie(pie2);
        modelAdapter.addPie(pie3);
        modelAdapter.addPie(pie4);
        modelAdapter.addPie(pie5);
        modelAdapter.addPie(pie6);
        modelAdapter.addPie(pie7);

        CamembertView camembertView = new CamembertView(modelAdapter);
        MyTableModel tableModel = new MyTableModel(modelAdapter);
        MyJtable myJtable = new MyJtable(tableModel, modelAdapter);

        // Create the controller and link the controller to the model...
        IController controller = new CamembertController(modelAdapter, camembertView, myJtable);
        camembertView.setController(controller);
        tableModel.setController(controller);
        myJtable.setController(controller);

        JScrollPane scrollPane = new JScrollPane(myJtable);

        // display layout
        GridLayout layout = new GridLayout(1, 2);

        window.getContentPane().add(controller.getView());
        window.getContentPane().add(scrollPane);

        window.setLocationRelativeTo(null);
        window.setLayout(layout);
        window.setVisible(true);
    }
}
