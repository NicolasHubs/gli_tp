import React from 'react';
import './App.css';
import MainStructure from '../component/MainStructure'

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <p className="App-title">Integration Week End</p>
            </header>
            <MainStructure/>
        </div>
    );
}

export default App;
