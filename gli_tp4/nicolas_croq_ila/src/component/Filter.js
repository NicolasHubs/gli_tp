import React from 'react';


class Filter extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.filter(e.target.value);
    }

    render() {
        return (
            <div className="filter">
                Filter:
                <select onChange={(e) => this.handleChange(e)}>
                    <option value="">All</option>
                    {this.props.processOptions()}
                </select>
            </div>
        )
    }
}

export default Filter;
