import React from 'react';
import AddExpense from "./AddExpense";
import TotalExpense from "./TotalExpense";

class Footer extends React.Component {


    render() {
        return (
            <div className="Footer">
                <AddExpense users={this.props.users} adder={this.props.adder} processOptions={this.props.processOptions}/>
                <TotalExpense costs={this.props.costs}/>
            </div>
        )
    }
}

export default Footer;
