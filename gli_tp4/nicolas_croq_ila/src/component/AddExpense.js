import React from 'react';

class AddExpense extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            adderWhat: "",
            adderWho: "",
            adderHow: -1
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClick(e) {
        if (this.state.adderWhat !== "" &&
            this.state.adderWho !== "" &&
            this.state.adderHow !== -1) {
            this.props.adder(this.state.adderWhat,
                this.state.adderWho,
                this.state.adderHow);
        }
    }

    handleChange(e, i) {
        if (i === 0)
            this.setState({adderWhat: e.target.value});
        else if (i === 1)
            this.setState({adderWho: e.target.value});
        else if (i === 2)
            this.setState({adderHow: parseFloat(e.target.value)})
    }

    render() {
        return (
            <div className="AddExpense">
                <input className="inputAdd" onChange={e => this.handleChange(e, 0)} placeholder="What?"/>
                <input className="inputAdd" onChange={e => this.handleChange(e, 1)} list="listUser"
                       placeholder="Who?"/>
                <input className="inputAddPrice" onChange={e => this.handleChange(e, 2)} placeholder="$$$"/>
                <button className="buttonAdd" onClick={this.handleClick}>+</button>
                <datalist id="listUser">
                    {this.props.processOptions()}
                </datalist>
            </div>
        )
    }
}

export default AddExpense;
