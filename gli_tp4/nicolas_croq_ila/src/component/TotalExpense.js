import React from 'react';


class TotalExpense extends React.Component {

    render() {
        let total = 0;
        for (let cost of this.props.costs) {
            total = total + parseFloat(cost.price);
        }
        return (
            <div className="TotalExpense">
                <p className="totalText">TOTAL EXPENSES</p>
                <p className="totalValue">{total} €</p>
            </div>
        )
    }
}

export default TotalExpense;
