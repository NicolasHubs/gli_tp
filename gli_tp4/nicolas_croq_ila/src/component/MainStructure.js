import React from 'react';
import Filter from "./Filter";
import ExpensesTable from "./ExpensesTable";
import Footer from "./Footer";

class MainStructure extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: "",
            users: ['Amine', 'Julie', 'Kévin'],
            costs: [{paidBy: 'Amine', title: 'Beer', price: 15.00},
                {paidBy: 'Julie', title: 'Lulz', price: 154.00},
                {paidBy: 'Kévin', title: 'Keks', price: 150.00}]
        }
    }

    filter = (user) => {
        this.setState({filter: user});
    };

    adder = (what, who, how) => {
        if (this.state.users.indexOf(who) === -1) {
            this.setState({users: [...this.state.users, who]});
            console.log("added user: " + who);
            console.log(this.state.users);
        }
        this.setState({costs: [...this.state.costs, {paidBy: who, title: what, price: how}]});
        console.log("added: " + what + " " + who + " " + how);
        console.log(this.state.costs);
    };

    processOptions = () => this.state.users.map(user => (<option key={user} value={user}>{user}</option>));

    render() {
        return (
            <div className="mainStructure">
                <Filter users={this.state.users} filter={this.filter} processOptions={this.processOptions}/>
                <ExpensesTable costs={this.state.costs} filter={this.state.filter}/>
                <Footer costs={this.state.costs} users={this.state.users} adder={this.adder} processOptions={this.processOptions}/>
            </div>
        )
    }
}

export default MainStructure;
