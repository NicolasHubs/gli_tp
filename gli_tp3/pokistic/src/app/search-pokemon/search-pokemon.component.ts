import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../data/pokemon';
import {PokedexService} from '../service/pokedex/pokedex.service';

@Component({
  selector: 'app-search-pokemon',
  templateUrl: './search-pokemon.component.html',
  styleUrls: ['./search-pokemon.component.scss']
})
export class SearchPokemonComponent implements OnInit {
  id: string;
  pokemonSelected: Pokemon;
  filter: string;
  isLoading = false;
  isError = false;
  listPokemon: Pokemon[] = [];
  idPokemon: string;

  constructor(private pokedexService: PokedexService) {
  }


  ngOnInit() {
    this.loadPokemons(100);
  }

  choosePokemon() {
    if (typeof this.id !== 'undefined' && this.id !== '') {
      this.idPokemon = this.id;
      this.id = '';
    } else if (typeof this.pokemonSelected.id !== 'undefined' && this.pokemonSelected.id) {
      console.log(this.pokemonSelected);
      this.idPokemon = this.pokemonSelected.id.toString();
      this.filter = '';
    } else {
      this.idPokemon = '';
    }
  }

  loadPokemons(limitLoading) {
    this.pokedexService.getPokemon(this.listPokemon.length, limitLoading)
      .then(pokemon => {
        this.listPokemon = this.listPokemon.concat(pokemon);
        this.isLoading = false;
        this.isError = false;
      })
      .catch(() => {
        this.isError = true;
        this.isLoading = false;
      });
  }
}
