import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PokisticRoutingModule } from './pokistic-routing.module';
import { PokisticComponent } from './pokistic.component';
import { PokemonNamePipe } from './pipe/pokemon-name.pipe';
import { SearchPokemonComponent } from './search-pokemon/search-pokemon.component';
import { HttpModule } from '@angular/http';
import { PokedexService } from './service/pokedex/pokedex.service';
import { InfoPokemonComponent } from './info-pokemon/info-pokemon.component';
import { CapitalizePipe } from './pipe/capitalize.pipe';

@NgModule({
  declarations: [
    PokisticComponent,
    PokemonNamePipe,
    SearchPokemonComponent,
    InfoPokemonComponent,
    CapitalizePipe
  ],
  imports: [
    BrowserModule,
    PokisticRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [PokedexService],
  bootstrap: [PokisticComponent]
})
export class PokisticModule { }
