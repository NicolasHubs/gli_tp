import {Component, Input, OnInit} from '@angular/core';
import {PokemonDetails} from '../data/pokemon-details';
import {PokedexService} from '../service/pokedex/pokedex.service';

@Component({
  selector: 'app-info-pokemon',
  templateUrl: './info-pokemon.component.html',
  styleUrls: ['./info-pokemon.component.scss']
})
export class InfoPokemonComponent implements OnInit {
  pokemonDetails: PokemonDetails;

  constructor(private pokedexService: PokedexService) {
  }

  ngOnInit() {
  }

  @Input()
  set id(id: string) {
    this.pokedexService.getPokemonDetails(parseInt(id, 10))
      .then(pokemonDetails => {
        this.pokemonDetails = pokemonDetails;
      });
  }
}
