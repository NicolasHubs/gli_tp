import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Pokemon} from '../../data/pokemon';
import {PokemonDetails} from '../../data/pokemon-details';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {
  private baseUrl = 'https://pokeapi.co/api/v2/pokemon/';
  private baseSpriteUrl = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

  constructor(private http: Http) {
  }

  getPokemon(offset: number, limit: number): Promise<Pokemon> {
    return this.http.get(`${this.baseUrl}?offset=${offset}&limit=${limit}`)
      .toPromise()
      .then(response => response.json().results)
      .then(items => items.map((item, idx) => {
        const id: number = idx + offset + 1;

        return {
          name: item.name,
          id
        };
      }));
  }

  getPokemonDetails(id: number): Promise<PokemonDetails> {
    let tmp;

    return this.http.get(`${this.baseUrl}${id}/`)
      .toPromise()
      .then(response => response.json())
      .then(details => {
        const types = details.types
          .map(t => {
            return t.type.name;
          });

        tmp = {
          id: details.id,
          name: details.name,
          weight: details.weight,
          sprite: `${this.baseSpriteUrl}${id}.png`,
          height: details.height,
          types
        };

        /**
         * We need to make another call
         * to get the description text.
         */
        return this.http
          .get(details.species.url)
          .toPromise();
      })
      .then(response => response.json())
      .then(species => {
        let description = '';
        let entries = species.flavor_text_entries;

        for (const entry of entries) {
          if (entry.language.name === 'fr') {
            description = entry.flavor_text;
            break;
          }
        }

        let name = '';
        entries = species.names;

        for (const entry of entries) {
          if (entry.language.name === 'fr') {
            name = entry.name;
            break;
          }
        }

        tmp.name  = name;
        tmp.description = description;
        return tmp;
      });
  }
}
