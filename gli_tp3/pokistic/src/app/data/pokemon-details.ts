export class PokemonDetails {
  id: string;
  name: string;
  weight: number;
  height: number;
  sprite: string;
  types: string[];
  description: string;

  constructor(id: string, name: string, weight: number, height: number, sprite: string, types: string[], description: string) {
    this.id = id;
    this.name = name;
    this.weight = weight;
    this.height = height;
    this.sprite = sprite;
    this.types = types;
    this.description = description;
  }
}
