import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PokisticComponent } from './pokistic.component';

describe('PokisticComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        PokisticComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(PokisticComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'pokistic'`, () => {
    const fixture = TestBed.createComponent(PokisticComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('pokistic');
  });

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(PokisticComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('pokistic app is running!');
  }));

});
